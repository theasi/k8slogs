from setuptools import find_packages, setup


setup(
    name="k8slogs",
    description="Aggregate logs from multiple kubernetes pods",
    author="Faculty Science",
    author_email="engineering@faculty.com",
    url="https://faculty.ai/",
    packages=find_packages(exclude=["tests.*"]),
    use_scm_version={"version_scheme": "post-release"},
    setup_requires=["setuptools_scm"],
    install_requires=["aiohttp", "pykube", "termcolor"],
    tests_require=[],
    entry_points={"console_scripts": ["k8slogs=k8slogs:cli"]},
)
