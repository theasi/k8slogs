#!/usr/bin/env python

import argparse
import asyncio
from enum import Enum
import functools
import json
import os
import signal
import ssl
import subprocess
import sys
from fnmatch import fnmatch
from urllib.parse import urlencode

import aiohttp
import dateutil.parser
import pykube
from termcolor import colored


LOG_LEVEL_COLOURS = {"info": "green", "warn": "yellow", "error": "red", "debug": "blue"}

TIME_UNITS = {"s": 1, "m": 60, "h": 60 * 60, "d": 60 * 60 * 24, "w": 60 * 60 * 24 * 7}


class CloudProvider(Enum):
    AWS = "AWS"
    GCP = "GCP"


CLOUD_PROVIDER: CloudProvider = CloudProvider[os.environ["CLOUD_PROVIDER"]]


@functools.total_ordering
class LogLine:
    def __init__(self, pod, timestamp, content):
        self.pod = pod
        self.timestamp = timestamp
        self.content = content

    def __lt__(self, other):
        if self.timestamp is None:
            return True
        if other.timestamp is None:
            return False
        return self.timestamp < other.timestamp

    def __eq__(self, other):
        return (
            self.pod == other.pod
            and self.timestamp == other.timestamp
            and self.content == other.content
        )


class Interrupter:
    def __init__(self, cancellable):
        self.called = False
        self.cancellable = cancellable

    def __call__(self, *args):
        self.called = True
        self.cancellable.cancel()


def get_api(context=None):
    config_file = os.path.expanduser("~/.kube/config")
    config = pykube.KubeConfig.from_file(config_file)
    if context is not None:
        config.set_current_context(context)
    api = pykube.HTTPClient(config)
    return api


def namespaces(api):
    return [namespace.name for namespace in pykube.Namespace.objects(api)]


def get_pods(api, name_pattern=None):
    for namespace in namespaces(api):
        for pod in pykube.Pod.objects(api, namespace=namespace):
            if name_pattern is None or fnmatch(pod.name, name_pattern):
                yield pod


def gcloud_access_token():
    try:
        gcloud_output = subprocess.check_output(
            ["gcloud", "config", "config-helper", "--format=json"]
        )
        return json.loads(gcloud_output)["credential"]["access_token"]
    except Exception as e:
        print("Error getting access token.")
        raise e


async def stream_logs_request(
    session, pod, container=None, follow=False, since_seconds=None
):
    log_call = "log"
    params = {"timestamps": True}
    if container:
        params["container"] = container
    if follow:
        params["follow"] = "true"
    if since_seconds is not None:
        params["sinceSeconds"] = since_seconds
    query_string = urlencode(params)
    log_call += "?{}".format(query_string) if query_string else ""
    kwargs = {"version": pod.version, "namespace": pod.namespace, "operation": log_call}

    url = pod.api.get_kwargs(**pod.api_kwargs(**kwargs))["url"]

    def get_request():
        if CLOUD_PROVIDER == CloudProvider.AWS:
            return session.get(url, timeout=None)
        elif CLOUD_PROVIDER == CloudProvider.GCP:
            headers = {"Authorization": f"Bearer {gcloud_access_token()}"}
            return session.get(url, headers=headers, timeout=None)

    async with get_request() as response:
        if response.status == 400:  # pod probably not ready
            return
        response.raise_for_status()
        accum = b""
        async for chunk in response.content.iter_chunked(1024):
            accum += chunk
            while b"\n" in accum:
                line, accum = accum.split(b"\n", maxsplit=1)
                yield line.decode()
        if accum:
            yield accum.decode()


async def pipe_logs_to_queue(session, pod, queue, follow, since_seconds):
    async for line in stream_logs_request(
        session, pod, follow=follow, since_seconds=since_seconds
    ):

        if line.strip():
            try:
                time_raw, body = line.split(maxsplit=1)
            except ValueError:
                try:
                    dateutil.parser.parse(line)
                except ValueError:
                    # Line is not a timestamp, take whole line as body
                    time = None
                    body = line
                else:
                    # Line is a timestamp with no body - skip it
                    continue
            else:
                time = dateutil.parser.parse(time_raw)
            await queue.put(LogLine(pod.name, time, body.strip()))


def format_json_message(body):
    message = body["message"]

    level = body.get("level")
    if level.lower() in LOG_LEVEL_COLOURS:
        level = colored(level, LOG_LEVEL_COLOURS[level.lower()])

    logger_name = body.get("logger_name")
    logger_name = f"[{logger_name}] " if logger_name else ""
    level = f"[{level}] " if level else ""
    return f"{level}{logger_name}{message}"


async def log_printer(queue, levels, show_pod_name):
    while True:
        line = await queue.get()
        try:
            body = json.loads(line.content)
            message = format_json_message(body)
            level = body.get("level")
            stack_trace = body.get("stack_trace")
        except (json.decoder.JSONDecodeError, KeyError):
            message = line.content
            level = None
            stack_trace = None
        if levels and (level is None or level.lower() not in levels):
            continue
        if show_pod_name:
            tpl = "{time} {pod} {message}"
        else:
            tpl = "{time} {message}"
        if line.timestamp:
            time = f"{line.timestamp:%Y-%m-%d %H:%M:%S}"
        else:
            time = "UNKNOWN"
        if len(message) > 4096:
            suffix = " [{}]".format(colored("TRUNCATED", "red"))
            message = message[:4096] + suffix
        print(
            tpl.format(
                time=colored(time, "cyan"),
                pod=colored(line.pod, "yellow"),
                message=message,
            )
        )
        if stack_trace:
            print(stack_trace)


async def stream_logs(pods, levels, follow, since_seconds, show_pod_name):

    queue = asyncio.PriorityQueue()

    ssl_context = ssl.create_default_context(
        cafile=pods[0].api.config.cluster["certificate-authority"].filename()
    )
    ssl_context.load_cert_chain(
        certfile=pods[0].api.config.user["client-certificate"].filename(),
        keyfile=pods[0].api.config.user["client-key"].filename()
    )
    connector = aiohttp.TCPConnector(ssl=ssl_context)

    async with aiohttp.ClientSession(connector=connector) as session:
        producers = [
            pipe_logs_to_queue(session, pod, queue, follow, since_seconds)
            for pod in pods
        ]
        printer = asyncio.ensure_future(log_printer(queue, levels, show_pod_name))

        try:
            await asyncio.gather(*producers)
        except asyncio.CancelledError:
            pass  # Continue with cancelling the printer

        printer.cancel()
        try:
            await printer
        except asyncio.CancelledError:
            pass  # Exit nicely


def parse_interval(since):
    try:
        unit = since[-1]
    except IndexError:
        raise ValueError

    value = int(since[:-1])

    try:
        return value * TIME_UNITS[unit.lower()]
    except KeyError:
        raise argparse.ArgumentTypeError(f"invalid time unit '{unit}'.")


def kubernetes_context(context):
    prefix = "kubernetes-"
    if not context.startswith(prefix):
        context = f"{prefix}{context}"

    suffix = ".asidata.science"
    if not context.endswith(suffix):
        context = f"{context}{suffix}"

    return context


def parse_name_pattern(pattern):
    if pattern == "":
        raise ValueError

    return f"*{pattern}*"


def cli():
    description = """
    View the merged log streams for some pods. The provided name will be
    matched as a substring of the name of all pods in the cluster, and the logs
    streams of all matching pods will be displayed.
    """

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "name",
        type=parse_name_pattern,
        help="Pattern matching name of pods to view logs for",
    )
    parser.add_argument("-c", "--context", type=kubernetes_context)
    parser.add_argument(
        "-l",
        "--levels",
        default=[],
        nargs="+",
        type=lambda s: s.lower(),
        help="List of log levels to show",
    )
    parser.add_argument("-f", "--follow", action="store_true")
    parser.add_argument(
        "-s", "--since", type=parse_interval, help="Show logs since specified time ago"
    )
    parser.add_argument(
        "--no-pod-name",
        action="store_false",
        dest="show_pod_name",
        help="Do not show the pod name in the output",
    )
    args = parser.parse_args()

    api = get_api(args.context)
    pods = list(get_pods(api, args.name))

    if not pods:
        print(f"no pods matching the pattern {args.name} found", file=sys.stderr)
        sys.exit(2)

    future = asyncio.ensure_future(
        stream_logs(pods, args.levels, args.follow, args.since, args.show_pod_name)
    )

    interrupter = Interrupter(future)

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, interrupter)
    loop.run_until_complete(future)
    loop.close()

    if interrupter.called:
        sys.exit(130)


if __name__ == "__main__":
    cli()
